/**
 * @ClassName: TestMapper
 * @Author: songgaoyuan
 * @Date: 2024-04-22  19:25
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import com.google.common.collect.Lists;
import com.sgy.stock.mapper.StockBusinessMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
public class TestMapper {

    @Autowired
    private StockBusinessMapper stockBusinessMapper;

    @Test
    public void t(){
        List<String> allStockCodes = stockBusinessMapper.getAllStockCodes();
//        System.out.println(allStockCodes);
        allStockCodes= allStockCodes.stream().map(code->code.startsWith("6")?"sh"+code:"sz"+code).collect(Collectors.toList());
        System.out.println(allStockCodes);

        //将所有个股编码组成的大的集合拆分成若干个集合40 ——》15 15 10
        Lists.partition(allStockCodes,15).forEach(codes->
        {
            System.out.println("size:"+codes.size()+":"+codes);
        } );
    }
}