/**
 * @ClassName: TestRestTemplate
 * @Author: songgaoyuan
 * @Date: 2024-04-21  14:02
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import com.google.gson.Gson;
import com.sgy.stock.service.StockTimerTaskService;
import lombok.Data;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;

@SpringBootTest
public class TestRestTemplate {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private StockTimerTaskService stockTimerTaskService;
    /**
     * 测试get请求携带url参数，访问外部接口
     */
    @Test
    public void t1(){
        String url="http://localhost:6766/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        /**
         * 参数1 URl请求地址
         * 参数2 请求返回的数据类型
         */
        ResponseEntity<String> resp = restTemplate.getForEntity(url, String.class);
        //获取相应头
        HttpHeaders headers = resp.getHeaders();
        System.out.println(headers.toString());
        //获取响应状态码
        HttpStatus statusCode = resp.getStatusCode();
        System.out.println(statusCode);
        //获取响应数据
        String body = resp.getBody();
        System.out.println(body);
    }

@Data
public static class Account{
    private Integer id;
    private String userName;
    private String address;
    }
    /**
     * get请求响应数据自动封装到vo对象
     *
     */
    @Test
    public void t2(){
        String url="http://localhost:6766/account/getByUserNameAndAddress?userName=zhangsan&address=sh";
        /**
         * 参数1 URL
         * 参数2 请求返回的数据类型
         */
        Account account = restTemplate.getForObject(url, Account.class);
        System.out.println(account);
    }
    /**
     * 请求头设置参数，访问指定接口
     */
    @Test
    public void t3(){
        String url="http://localhost:6766/account/getHeader";
        //设置请求头参数
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("userName","zhansan");
        //请求头填充到请求对象下
        HttpEntity<Object> entity = new HttpEntity<>(httpHeaders);
        //发送请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        String result = responseEntity.getBody();
        System.out.println(result);
    }
    /**
     * post模拟form表单提交数据
     *
     */

    @Test
    public void t4(){
        String url="http://localhost:6766/account/addAccount";
        //设置请求头，指定请求数据格式
        HttpHeaders headers = new HttpHeaders();
        //告知被调用方，请求方式为form表单提交。使其按照规定方式解析处理
        headers.add("Content-type","application/x-www-form-urlencoded");
        //组装模拟form表单提交数据，内部元素相当于form表单的input框
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        map.add("id","10");
        map.add("userName","sgy");
        map.add("address","shanghai");
        HttpEntity<LinkedMultiValueMap<String,Object>> httpEntity = new HttpEntity<>(map,headers);
        /**
         * 参数1 URL
         * 参数2 请求方式 post
         * 参数3 请求体对象
         * 参数4 响应数据类型
         */
        ResponseEntity<Account> exchange = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Account.class);
        Account body = exchange.getBody();
        System.out.println(body);
    }
    /**
     * post发送json数据
     *
     */
    @Test
    public void t5(){
        String url="http://localhost:6766/account/updateAccount";
        //设置请求头的请求参数类型
        HttpHeaders headers = new HttpHeaders();
        //告知被调用方，发送的数据格式的json格式，对方要以json的方式解析处理
        headers.add("Content-type","application/json; charset=utf-8");
        //组装json格式数据
//        HashMap<String,String> reqMap=new HashMap<>();
//        reqMap.put("id","1");
//        reqMap.put("userName","zhangsan");
//        reqMap.put("address","上海");
//        String jsonReqData=new Gson().toJson(reqMap);
        String jsonReq="{\"address\":\"上海\",\"id\":\"1\",\"userName\":\"zhangsan\"}";
        //构建请求对象
        HttpEntity<String> httpEntity = new HttpEntity<>(jsonReq,headers);
        /**
         * 发送数据
         * 参数1 请求URL地址
         * 参数2 请求方式
         * 参数3 请求体对象，携带了请求头和请求相关的参数
         * 参数4 响应数据类型
         */
        ResponseEntity<Account> responseEntity = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Account.class);
        Account body = responseEntity.getBody();
        System.out.println(body);
        //1
        //2


    }
    /**
     *
     * 获取请求Cookie值
     */

    @Test
    public void t6(){
        String url="http://localhost:6766/account/getCookie";
        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
        //获取Cookie
        List<String> cookies = result.getHeaders().get("Set-Cookie");
        String body = result.getBody();
        System.out.println(body);
        System.out.println(cookies);
    }
    /**
     * 测试采集国内内大盘数据
     */
    @Test
    public void t7() throws InterruptedException {
//        stockTimerTaskService.getInnerMarketInfo();//采集大盘数据
        stockTimerTaskService.getStockRtIndex();
        //让主线程休眠，等待子线程执行完任务
        Thread.sleep(5000);
    }
}