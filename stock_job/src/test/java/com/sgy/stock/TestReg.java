/**
 * @ClassName: TestReg
 * @Author: songgaoyuan
 * @Date: 2024-04-22  13:46
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestReg {
    @Test
    public void t(){
//        String content = "I am noob from runoob.com.";
//
//        String pattern = ".*runoob.*";
//        //创建Pattern对象
//        Pattern r = Pattern.compile(pattern);
//        boolean isMatch = Pattern.matches(pattern, content);
//        System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);
//
//
//        Matcher matcher = r.matcher(content);
//        int i = matcher.groupCount();
//        System.out.println(i);
//        if(matcher.find()){
//            System.out.println("1："+ matcher.group(0));
//
//        }
        // 按指定模式在字符串查找
        String line = "This order was placed for QT3000! OK?";
        String pattern = "(\\D*)(\\d+)(.*)()()";

        // 创建 Pattern 对象
        Pattern r = Pattern.compile(pattern);

        // 现在创建 matcher 对象
        Matcher m = r.matcher(line);
        System.out.println(m.groupCount());
        String group = m.group();
        if (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
            System.out.println("Found value: " + m.group(1) );
            System.out.println("Found value: " + m.group(2) );
            System.out.println("Found value: " + m.group(3) );
            System.out.println("Found value: " + m.group(4) );
        } else {
            System.out.println("NO MATCH");
        }
    }
    }
