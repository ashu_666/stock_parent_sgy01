/**
 * @ClassName: CommonConfig
 * @Author: songgaoyuan
 * @Date: 2024-04-15  20:14
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.config;

import com.sgy.stock.pojo.vo.StockInfoConfig;
import com.sgy.stock.utils.IdWorker;
import com.sgy.stock.utils.ParserStockInfoUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableConfigurationProperties({StockInfoConfig.class })//开启相关对象配置类
public class CommonConfig {
    /**
     * 密码加密 匹配器
     * BCryptPasswordEncoder方法采用SHA-256对密码进行加密
     * @return
     */
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }

    /**
     * 配置id生成器bean
     * @return
     */
    @Bean
    public IdWorker idWorker(){
        //基于运维人员对机房和机器的编号规划自行约定
        return new IdWorker(1l,2l);
    }
    /**
     * 定义解析股票 大盘 外盘 个股 板块相关信息的工具类bean
     */
    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(){
        return new ParserStockInfoUtil(new IdWorker());
    }
}