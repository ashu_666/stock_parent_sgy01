/**
 * @ClassName: TaskExecutePoolConfig
 * @Author: songgaoyuan
 * @Date: 2024-05-07  19:24
 * @Description: 定义线程池的配置类
 * @Version: 1.0
 */
package com.sgy.stock.config;

import com.sgy.stock.pojo.vo.TaskThreadPoolInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class TaskExecutePoolConfig {



    @Autowired
    private TaskThreadPoolInfo info;
    /**
     * 构建线程池bean对象
     */
    @Bean(name = "threadPoolTaskExecutor",destroyMethod ="shutdown" )
    public ThreadPoolTaskExecutor threadPoolTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        //设置核心线程数
        executor.setCorePoolSize(info.getCorePoolSize());
        //设置最大线程数
        executor.setMaxPoolSize(info.getMaxPoolSize());
        //设置空闲线程最大存活时间
        executor.setKeepAliveSeconds(info.getKeepAliveSeconds());
        //设置任务队列长度
        executor.setQueueCapacity(info.getQueueCapacity());
        //设置拒绝策略
//        executor.setRejectedExecutionHandler();


        executor.initialize();


        return executor;
    }
}