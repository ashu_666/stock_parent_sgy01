/**
 * @ClassName: HttpClientConfig
 * @Author: songgaoyuan
 * @Date: 2024-04-21  13:48
 * @Description: 定义Http客户端工具bean
 * @Version: 1.0
 */
package com.sgy.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HttpClientConfig {
    /**
     * 定义restTemplate bean
     * 定义Http客户端bean
     * @return
     */
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }


}