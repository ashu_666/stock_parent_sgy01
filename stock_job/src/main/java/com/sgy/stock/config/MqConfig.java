/**
 * @ClassName: MqConfig
 * @Author: songgaoyuan
 * @Date: 2024-04-23  11:12
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.config;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration

public class MqConfig {

    /**
     * 重新定义消息序列化的方式，改为基于json格式序列化和反序列化
     *
     */
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
    /**
     * 国内大盘信息队列
     *定义队列
     */
    @Bean
    public Queue innerMarketQueue(){
        return new Queue("innerMarketQueue",true);
    }
    /**
     * 定义路由股票信息的交换机
     * 主题交换机
     */
    @Bean
    public TopicExchange stockTopicExchange(){
        return new TopicExchange("stockExchange",true,false);
    }
    /**
     * 绑定队列到指定交换机上
     */
    @Bean
    public Binding bindingInnerMarketToStockExchange(){
        return BindingBuilder.bind(innerMarketQueue()).to(stockTopicExchange()).with("inner.market");
    }
}