/**
 * @ClassName: StockTimerTaskService
 * @Author: songgaoyuan
 * @Date: 2024-04-22  11:17
 * @Description: 定义股票采集数据服务接口
 * @Version: 1.0
 */
package com.sgy.stock.service;

public interface StockTimerTaskService {


    /**
     *获取国内大盘的实时数据信息
     */
    void getInnerMarketInfo();
    /**
     * 定义获取分钟级股票数据
     */
    void getStockRtIndex();
    /**
     * 获取板块数据
     */
}