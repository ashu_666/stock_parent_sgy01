/**
 * @ClassName: StockTimerTaskServiceImpl
 * @Author: songgaoyuan
 * @Date: 2024-04-22  11:21
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.service.impl;

import com.google.common.collect.Lists;
import com.sgy.stock.constant.StockConstant;
import com.sgy.stock.mapper.StockBusinessMapper;
import com.sgy.stock.mapper.StockMarketIndexInfoMapper;
import com.sgy.stock.mapper.StockRtInfoMapper;
import com.sgy.stock.pojo.entity.StockMarketIndexInfo;
import com.sgy.stock.pojo.entity.StockRtInfo;
import com.sgy.stock.pojo.vo.StockInfoConfig;
import com.sgy.stock.service.StockTimerTaskService;
import com.sgy.stock.utils.DateTimeUtil;
import com.sgy.stock.utils.IdWorker;
import com.sgy.stock.utils.ParseType;
import com.sgy.stock.utils.ParserStockInfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.record.DVALRecord;
import org.checkerframework.checker.units.qual.A;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class StockTimerTaskServiceImpl implements StockTimerTaskService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private StockInfoConfig stockInfoConfig;
    @Autowired
    private IdWorker idWorker;
    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @Autowired
    private StockBusinessMapper stockBusinessMapper;
    @Autowired
    private ParserStockInfoUtil parserStockInfoUtil;
    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Override
    public void getInnerMarketInfo() {
        //1.采集原始数据
        //http://hq.sinajs.cn/list=sh000001,sz399001
        String url= stockInfoConfig.getMarketUrl()+String.join(",",stockInfoConfig.getInner());
        //维护请求头，添加防盗链和用户标识
        HttpHeaders headers = new HttpHeaders();
        headers.add("Referer","https://finance.sina.com.cn/stock/");
        //维护Http请求实体对象
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        //发送请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
        int statusCodeValue=responseEntity.getStatusCodeValue();
        if(statusCodeValue!=200){
            log.error("当前时间点：{}，采集数据失败，Http状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);

            //其它：发送邮件什么的通知想相关人员
            return;
        }
        //获取JS格式数据
        String jsData = responseEntity.getBody();
        log.info("当前时间点：{}，采集数据：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),jsData);


        //2.Java正则解析原始数据
        String reg="var hq_str_(.+)=\"(.+)\";";
        //编译表达式,获取编译对象
        Pattern pattern = Pattern.compile(reg);
        //匹配字符串
        Matcher matcher = pattern.matcher(jsData);
        ArrayList<StockMarketIndexInfo> list = new ArrayList<>();
        //判断是否有匹配的数值
        while (matcher.find()){
            //获取大盘的code
            String marketCode = matcher.group(1);
            //获取其它信息，字符串以逗号间隔
            String otherInfo=matcher.group(2);
            //以逗号切割字符串，形成数组
            String[] splitArr = otherInfo.split(",");
            //大盘名称
            String marketName=splitArr[0];
            //获取当前大盘的开盘点数
            BigDecimal openPoint=new BigDecimal(splitArr[1]);
            //前收盘点
            BigDecimal preClosePoint=new BigDecimal(splitArr[2]);
            //获取大盘的当前点数
            BigDecimal curPoint=new BigDecimal(splitArr[3]);
            //获取大盘最高点
            BigDecimal maxPoint=new BigDecimal(splitArr[4]);
            //获取大盘的最低点
            BigDecimal minPoint=new BigDecimal(splitArr[5]);
            //获取成交量
            Long tradeAmt=Long.valueOf(splitArr[8]);
            //获取成交金额
            BigDecimal tradeVol=new BigDecimal(splitArr[9]);
            //时间
            Date curTime = DateTimeUtil.getDateTimeWithoutSecond(splitArr[30] + " " + splitArr[31]).toDate();
            //组装entity对象
            StockMarketIndexInfo info = StockMarketIndexInfo.builder()
                    .id(idWorker.nextId())
                    .marketCode(marketCode)
                    .marketName(marketName)
                    .curPoint(curPoint)
                    .openPoint(openPoint)
                    .preClosePoint(preClosePoint)
                    .maxPoint(maxPoint)
                    .minPoint(minPoint)
                    .tradeVolume(tradeVol)
                    .tradeAmount(tradeAmt)
                    .curTime(curTime)
                    .build();
            //收集封装的对象，方便批量插入
            list.add(info);
        }
        log.info("采集的当前大盘数据：{}",list);
        //批量插入
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        //3.解析后封装entity
        //4.调用mybatis批量入库
        int count= stockMarketIndexInfoMapper.insertBatch(list);
        if(count>0){
            //大盘数据采集完毕后，通知backend工程刷新缓存
            //发送日期对象，接收方通过接受的日期与当前日期对比，来判断数据延迟的时长，用于运维通知处理，方便后期优化。
            rabbitTemplate.convertAndSend("stockExchange","inner.market",new Date());
            log.info("当前时间:{}，插入大盘数据:{}，{}插入成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);

        }else{
            log.info("当前时间:{}，插入大盘数据:{}，{}插入失败",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
        }
    }
    /**
     * 定义获取分钟级股票数据
     */
    @Override
    public void getStockRtIndex() {
        //1、获取所有个股集合 3000+
        // 组装URL地址
        List<String> allStockCodes = stockBusinessMapper.getAllStockCodes();
//        System.out.println(allStockCodes);
        allStockCodes= allStockCodes.stream().map(code->code.startsWith("6")?"sh"+code:"sz"+code).collect(Collectors.toList());
        System.out.println(allStockCodes);
        long startTime = System.currentTimeMillis();
        //将所有个股编码组成的大的集合拆分成若干个集合40 ——》15 15 10
        //将大的集合切分成若干个小集合，分批次拉取数据
        Lists.partition(allStockCodes,15).forEach(codes->
        {

            //原始方法
//            //分批次采集
//            String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
//            //维护请求头，添加防盗链和用户标识
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Referer","https://finance.sina.com.cn/stock/");
//            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//            //发送请求
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//
//            int statusCodeValue = responseEntity.getStatusCodeValue();
//            if(statusCodeValue!=200){
//                log.error("当前时间点：{}，采集数据失败，Http状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
//                return;
//            }
//            String jsData = responseEntity.getBody();
//            //调用工具类解析获取各个数据
//            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//            log.info("采集个股数据：{}",list);
//            //todo：批量插入保存
//            int count=stockRtInfoMapper.insertBatch(list);
//            if(count>0){
//                log.info("当前时间{}，插入个股数据{}，{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }else {
//                log.error("当前时间{}，插入个股数据{}，{}失败",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }
//            //原始方案采集个股数据时将集合分片，然后分批次串行采集数据，效率不高，存在较高的采集延迟
//
//            //引入多线程


            //方案1  线程
//            new Thread(()->{
//                            String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
//            //维护请求头，添加防盗链和用户标识
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Referer","https://finance.sina.com.cn/stock/");
//            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
//            //发送请求
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
//
//            int statusCodeValue = responseEntity.getStatusCodeValue();
//            if(statusCodeValue!=200){
//                log.error("当前时间点：{}，采集数据失败，Http状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
//                return;
//            }
//            String jsData = responseEntity.getBody();
//            //调用工具类解析获取各个数据
//            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
//            log.info("采集个股数据：{}",list);
//            //todo：批量插入保存
//            int count=stockRtInfoMapper.insertBatch(list);
//            if(count>0){
//                log.info("当前时间{}，插入个股数据{}，{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }else {
//                log.error("当前时间{}，插入个股数据{}，{}失败",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
//            }
//            });



            //方案2 线程池；
            //每个分片的数据开启一个线程异步执行任务
            threadPoolTaskExecutor.execute(()->{
                String url=stockInfoConfig.getMarketUrl()+String.join(",",codes);
            //维护请求头，添加防盗链和用户标识
            HttpHeaders headers = new HttpHeaders();
            headers.add("Referer","https://finance.sina.com.cn/stock/");
            HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
            //发送请求
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);

            int statusCodeValue = responseEntity.getStatusCodeValue();
            if(statusCodeValue!=200){
                log.error("当前时间点：{}，采集数据失败，Http状态码：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),statusCodeValue);
                return;
            }
            String jsData = responseEntity.getBody();
            //调用工具类解析获取各个数据
            List<StockRtInfo> list = parserStockInfoUtil.parser4StockOrMarketInfo(jsData, ParseType.ASHARE);
            log.info("采集个股数据：{}",list);
            //todo：批量插入保存
            int count=stockRtInfoMapper.insertBatch(list);
            if(count>0){
                log.info("当前时间{}，插入个股数据{}，{}成功",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
            }else {
                log.error("当前时间{}，插入个股数据{}，{}失败",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),list);
            }


            });





        }
        );

        long t=System.currentTimeMillis()-startTime;

        log.info("本次采集花费时间：{}ms",t);

    }

}