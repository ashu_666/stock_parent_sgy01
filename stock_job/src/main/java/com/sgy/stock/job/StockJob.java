/**
 * @ClassName: StockJob
 * @Author: songgaoyuan
 * @Date: 2024-05-07  15:38
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.job;


import com.sgy.stock.service.StockTimerTaskService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockJob {

    /**
     * 注入股票定时任务服务bean
     */
    @Autowired
    private StockTimerTaskService stockTimerTaskService;

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("myJobHandler")
    public void demoJobHandler() throws Exception {
        XxlJobHelper.log("XXL-JOB, Hello World.");
        System.out.println("当前时间点为："+ DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
        // default success
    }

    /**
     * 定时采集A股大盘数据
     */
    @XxlJob("getInnerMarketInfo")
    public void getStockInnerMarketInfos(){
        System.out.println("进来了");
        stockTimerTaskService.getInnerMarketInfo();

    }

    /**
     * 定时采集A股个股数据
     */
    @XxlJob("getStockRtIndex")
    public void getStockRtIndex(){
        System.out.println("进来了");
        stockTimerTaskService.getStockRtIndex();

    }

}