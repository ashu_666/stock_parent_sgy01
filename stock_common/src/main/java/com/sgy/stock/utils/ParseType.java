/**
 * @ClassName: ParseType
 * @Author: songgaoyuan
 * @Date: 2024-04-22  21:51
 * @Description:  股票信息采集数据类型标识
 * @Version: 1.0
 */
package com.sgy.stock.utils;

public class ParseType {

    /**
     * A股大盘标识
     */

    /**
     * 国外大盘标识
     */
    public static final int OUTER=2;

    /**
     * A股标识
     */
    public static final int ASHARE=3;

}
