package com.sgy.stock.pojo.entity;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 权限表（菜单）
 * @TableName sys_permission
 */
@ApiModel(description = "权限表（菜单）")
@Data
public class SysPermission implements Serializable {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", position = 1)
    private Long id;

    /**
     * 菜单权限编码(前端按钮权限标识)
     */
    @ApiModelProperty(value = "菜单权限编码(前端按钮权限标识)", position = 2)
    private String code;

    /**
     * 菜单权限名称
     */
    @ApiModelProperty(value = "菜单权限名称", position = 3)
    private String title;

    /**
     * 菜单图标(侧边导航栏图标)
     */
    @ApiModelProperty(value = "菜单图标(侧边导航栏图标)", position = 4)
    private String icon;

    /**
     * SpringSecurity授权标识(如：sys:user:add)
     */
    @ApiModelProperty(value = "SpringSecurity授权标识(如：sys:user:add)", position = 5)
    private String perms;

    /**
     * 访问地址URL
     */
    @ApiModelProperty(value = "访问地址URL", position = 6)
    private String url;

    /**
     * 资源请求类型
     */
    @ApiModelProperty(value = "资源请求类型", position = 7)
    private String method;

    /**
     * name与前端vue路由name约定一致
     */
    @ApiModelProperty(value = "name与前端vue路由name约定一致", position = 8)
    private String name;

    /**
     * 父级菜单权限id，pid等于0 为顶层权限
     */
    @ApiModelProperty(value = "父级菜单权限id，pid等于0 为顶层权限", position = 9)
    private Long pid;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序", position = 10)
    private Integer orderNum;

    /**
     * 菜单权限类型(1:目录;2:菜单;3:按钮)
     */
    @ApiModelProperty(value = "菜单权限类型(1:目录;2:菜单;3:按钮)", position = 11)
    private Integer type;

    /**
     * 状态1:正常 0：禁用
     */
    @ApiModelProperty(value = "状态1:正常 0：禁用", position = 12)
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", position = 13)
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", position = 14)
    private Date updateTime;

    /**
     * 是否删除(1未删除；0已删除)
     */
    @ApiModelProperty(value = "是否删除(1未删除；0已删除)", position = 15)
    private Integer deleted;

    @ApiModelProperty(hidden = true)
    private static final long serialVersionUID = 1L;
}