/**
 * @ClassName: StockInfoConfig
 * @Author: songgaoyuan
 * @Date: 2024-04-18  13:51
 * @Description: 定义股票相关的值对象封装
 * @Version: 1.0
 */
package com.sgy.stock.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
@ApiModel
@Data
@ConfigurationProperties(prefix = "stock")
@Component
public class StockInfoConfig {
    //封装国内A股大盘编码集合
    @ApiModelProperty("封装国内A股大盘编码集合")
    private List<String> inner;
    //外盘编码集合
    @ApiModelProperty("外盘编码集合")
    private List<String> outer;
/**
 * 统计最新交易时间点下股票在各个涨幅区间的数量
 * @return
 */
    @ApiModelProperty("股票涨幅区间标题集合")
    private List<String> upDownRange;

    //大盘 外盘 个股的公共URL
    @ApiModelProperty(value = "大盘 外盘 个股的公共URL")
    private String marketUrl;
    //板块参数获取url
    @ApiModelProperty(value = "板块参数获取url")
    private String blockUrl;

}