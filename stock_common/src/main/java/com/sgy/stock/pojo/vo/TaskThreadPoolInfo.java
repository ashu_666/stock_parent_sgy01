/**
 * @ClassName: TaskThreadPoolInfo
 * @Author: songgaoyuan
 * @Date: 2024-05-07  19:19
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.pojo.vo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "task.pool")
@Data
public class TaskThreadPoolInfo {
    /**
     *  核心线程数（获取硬件）：线程池创建时候初始化的线程数
     */
    private Integer corePoolSize;
    private Integer maxPoolSize;
    private Integer keepAliveSeconds;
    private Integer queueCapacity;
}