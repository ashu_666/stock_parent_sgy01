/**
 * @ClassName: User
 * @Author: songgaoyuan
 * @Date: 2024-04-20  09:43
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.pojo.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements Serializable {
    @ExcelProperty(value = "用户名",index = 0)
    private String userName;
    @ExcelProperty(value = "年龄",index = 1)

    private Integer age;
    @ExcelProperty(value = "地址",index = 2)

    private String address;
    @ExcelProperty(value = "生日",index = 3)
    @DateTimeFormat("yyyy/MM/dd")
    private Date birthday;
}