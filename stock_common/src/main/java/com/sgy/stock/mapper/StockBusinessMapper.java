package com.sgy.stock.mapper;

import com.sgy.stock.pojo.entity.StockBusiness;

import java.util.List;

/**
* @author 8615290344206
* @description 针对表【stock_business(主营业务表)】的数据库操作Mapper
* @createDate 2024-04-15 14:59:25
* @Entity com.sgy.stock.pojo.entity.StockBusiness
*/
public interface StockBusinessMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockBusiness record);

    int insertSelective(StockBusiness record);

    StockBusiness selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockBusiness record);

    int updateByPrimaryKey(StockBusiness record);
    /**
     * 获取所有A股个股的编码集合
     */
    List<String> getAllStockCodes();
}
