package com.sgy.stock.mapper;

import com.sgy.stock.pojo.domain.Stock4EvrDayDomain;
import com.sgy.stock.pojo.domain.Stock4MinuteDomain;
import com.sgy.stock.pojo.domain.StockUpdownDomain;
import com.sgy.stock.pojo.entity.StockRtInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author 8615290344206
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2024-04-15 14:59:25
* @Entity com.sgy.stock.pojo.entity.StockRtInfo
*/
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    List<StockUpdownDomain> getNewestStockInfo(@Param("timePoint")Date curDate);
    /**
     * @author sgy
     * @date 2024-04-19 20:47:42
     * @desc 统计指定日期范围内股票涨停或者跌停的数量流水
     */
    /**
     * 查询指定时间范围内每分钟涨停或者跌停的数量
     * @param openTime 开始时间
     * @param curTime 结束时间 一般开始时间和结束时间在同一天
     * @param flag 约定:1->涨停 0:->跌停
     * @return
     */
    List<Map> getStockUpdownCount(@Param("openTime") Date openTime, @Param("curTime") Date curTime, @Param("flag") int flag);
    /**
     *统计指定时间点下股票在各个涨跌区间下的数量
     * @
     */
    List<Map> getIncreaseRangeInfoByDate(@Param("dateTime") Date curDate);

    /**
     * 根据时间范围查询指定股票的交易流水
     * @param stockCode 股票code
     * @param startTime 起始时间
     * @param endTime 终止时间
     * @return
     */
    List<Stock4MinuteDomain> getStockInfoByCodeAndDate(@Param("stockCode") String stockCode, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

    List<Stock4EvrDayDomain> getStockInfo4EvrDay(@Param("stockCode") String stockCode, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
    /**
     * 批量插入个股数据
     */
    int insertBatch(@Param("list") List<StockRtInfo> list);
}
