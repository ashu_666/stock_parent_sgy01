package com.sgy.stock.mapper;

import com.sgy.stock.pojo.entity.SysRole;

/**
* @author 8615290344206
* @description 针对表【sys_role(角色表)】的数据库操作Mapper
* @createDate 2024-04-15 14:59:25
* @Entity com.sgy.stock.pojo.entity.SysRole
*/
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

}
