package com.sgy.stock.mapper;

import com.sgy.stock.pojo.entity.SysLog;

/**
* @author 8615290344206
* @description 针对表【sys_log(系统日志)】的数据库操作Mapper
* @createDate 2024-04-15 14:59:25
* @Entity com.sgy.stock.pojo.entity.SysLog
*/
public interface SysLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

}
