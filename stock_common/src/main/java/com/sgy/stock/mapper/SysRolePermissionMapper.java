package com.sgy.stock.mapper;

import com.sgy.stock.pojo.entity.SysRolePermission;

/**
* @author 8615290344206
* @description 针对表【sys_role_permission(角色权限表)】的数据库操作Mapper
* @createDate 2024-04-15 14:59:25
* @Entity com.sgy.stock.pojo.entity.SysRolePermission
*/
public interface SysRolePermissionMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRolePermission record);

    int insertSelective(SysRolePermission record);

    SysRolePermission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRolePermission record);

    int updateByPrimaryKey(SysRolePermission record);

}
