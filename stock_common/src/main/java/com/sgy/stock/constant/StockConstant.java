/**
 * @ClassName: StockConstant
 * @Author: songgaoyuan
 * @Date: 2024-04-17  13:05
 * @Description: 常量类信息封装
 * @Version: 1.0
 */
package com.sgy.stock.constant;

public class StockConstant {
    //定义校验码的前缀
    public static final String CHECK_PREFIX="CK";
    //Http请求头携带Token信息key
    public static final String TOKEN_HEADER="authorization";

    //缓存股票相关信息的cacheName命名前缀
    public static final String STOCK="stock";



}