/**
 * @ClassName: TestExcel
 * @Author: songgaoyuan
 * @Date: 2024-04-20  09:41
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.sgy.stock.pojo.entity.SysUser;
import com.sgy.stock.pojo.entity.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestExcel {


    public List<User> init(){

        //组装数据
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setAddress("上海"+i);
            user.setUserName("张三"+i);
            user.setBirthday(new Date());
            user.setAge(10+i);
            users.add(user);
        }
        return users;
    }
    @Test
    public void tt(){

        List<User> users = init();
        EasyExcel.write("D:\\day01\\用户1.xls", User.class).sheet("用户信息").doWrite(users);
    }
    @Test
    public void t2(){
        /**
         * excel数据格式必须与实体类定义一致，否则数据读取不到
         */
            ArrayList<User> users = new ArrayList<>();
            //读取数据
            EasyExcel.read("D:\\day01\\用户1.xls", User.class, new AnalysisEventListener<User>() {
               /**
                * @author sgy
                * @param:
                * @desc 逐行读取，并封装，
                * @return
                */
                @Override
                public void invoke(User o, AnalysisContext analysisContext) {
                    System.out.println(o);
                    users.add(o);
                }
                /**
                 * @author sgy
                 * @param: context
                 * @desc 所有行读取完毕后，回调方法
                 * @return
                 */
                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                    System.out.println("完成。。。。");
                }
            }).sheet().doRead();
            System.out.println(users);
        }
    }
