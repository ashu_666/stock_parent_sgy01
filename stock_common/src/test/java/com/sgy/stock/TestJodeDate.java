/**
 * @ClassName: TestJodeDate
 * @Author: songgaoyuan
 * @Date: 2024-04-17  22:26
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import com.sgy.stock.utils.DateTimeUtil;
import org.joda.time.DateTime;
import org.junit.Test;

//@SpringBootTest()
public class TestJodeDate {
    @Test
    public void testJode(){
        //获取指定时间下最近的股票有效交易时间
        //周末--->20220107145800
        DateTime target = DateTime.now().withDate(2022, 1, 9);
        //周六--->20220107145800
        target = DateTime.now().withDate(2022,12, 8);
        //周一 上午九点--->20220107145800
        target= DateTime.now().withDate(2022, 12, 10).withHourOfDay(9).withMinuteOfHour(0);
        //开盘 上午 9:30 到11点半--->20220110094058
        target=DateTime.now().withDate(2022, 12, 10).withHourOfDay(9).withMinuteOfHour(40);
        //中午休盘：11:30 到 13:00之间--->20220110113120
        target=DateTime.now().withDate(2022, 12 ,10).withHourOfDay(12).withMinuteOfHour(40);
        //开盘 下午13:00 到15:00-->20220110144012
        target=DateTime.now().withDate(2022, 12 ,10).withHourOfDay(14).withMinuteOfHour(40);
        //停盘 15:00后-->20220110145800
        target=DateTime.now().withDate(2022, 1, 10).withHourOfDay(15).withMinuteOfHour(40);
        String leastDateTime = DateTimeUtil.getLastDateString4Stock(target);
        System.out.println(leastDateTime);
    }

}