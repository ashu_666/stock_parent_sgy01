/**
 * @ClassName: test
 * @Author: songgaoyuan
 * @Date: 2024-04-15  20:18
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class test {
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * @author sgy
     * @date 2024-04-15 20:20:10
     * @desc    测试密码加密
     */

    @Test
    public void test01(){
        String pwd="12345";
        String encodePwd = passwordEncoder.encode(pwd);
        System.out.println(encodePwd);

    }

    @Test
    public void test02(){
        String pwd="12345";
        String enpwd="$2a$10$MMC29Emlsj2yb5pzcPUZ/Om4nR0fwemaB0lwnQeS1I6ylUPGY.cl6";
        boolean matches = passwordEncoder.matches(pwd, enpwd);
        System.out.println(matches);
    }
    @Test
    public void getmarketids(){

    }
}