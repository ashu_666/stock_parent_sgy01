/**
 * @ClassName: TestPageHelper
 * @Author: songgaoyuan
 * @Date: 2024-04-19  16:12
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgy.stock.mapper.SysUserMapper;
import com.sgy.stock.pojo.entity.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
@SpringBootTest
public class TestPageHelper {
    @Autowired
    private SysUserMapper sysUserMapper;
    /**
     * @author sgy
     * @date 2024-04-19 16:17:53
     * @desc 测试分页
     */
    @Test
    public void t(){
        Integer page=2;//当前页
        Integer pageSize=5;//每页大小
        PageHelper.startPage(page,pageSize);
        List<SysUser> all = sysUserMapper.findAll();

        //将查询的page对象封装到PageInfo下就可以获取分页的各种数据
        PageInfo<SysUser> pageInfo = new PageInfo<>(all);
        int pageNum = pageInfo.getPageNum();//获取当前页
        int pageSize1 = pageInfo.getPageSize();//获取每页大小
        int pages = pageInfo.getPages();//获取总页数
        int size = pageInfo.getSize();//获取当前页的记录数
        long total = pageInfo.getTotal();//总记录书
        List<SysUser> list = pageInfo.getList();//获取当前页的内容
        System.out.println(all);
    }

}