/**
 * @ClassName: TestReids
 * @Author: songgaoyuan
 * @Date: 2024-04-16  11:43
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
public class TestReids {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Test
    public void test01(){
        //存入值
        redisTemplate.opsForValue().set("myname","zhangsan");

        String myname = redisTemplate.opsForValue().get("myname");
        System.out.println(myname);
    }
}