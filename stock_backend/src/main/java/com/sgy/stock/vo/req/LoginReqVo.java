/**
 * @ClassName: LoginReqVo
 * @Author: songgaoyuan
 * @Date: 2024-04-15  19:57
 * @Description: 登录请求vo
 * @Version: 1.0
 */
package com.sgy.stock.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel
public class LoginReqVo {
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty(value="用户明文密码")
    private String password;
    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码")
    private String code;
    /**
     * 会话Id
     * */
    @ApiModelProperty(value = "会话Id")
    private String sessionId;
}