/**
 * @ClassName: StockMQMsgListener
 * @Author: songgaoyuan
 * @Date: 2024-04-23  14:44
 * @Description: 定义股票相关mq消息监听
 * @Version: 1.0
 */
package com.sgy.stock.mq;

import com.github.benmanes.caffeine.cache.Cache;
import com.sgy.stock.mapper.StockMarketIndexInfoMapper;
import com.sgy.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
@Slf4j
@Component
public class StockMQMsgListener {
    @Autowired
    private Cache<String,Object> caffeineCache;
    @Autowired
    private StockService stockService;
    @RabbitListener(queues = "innerMarketQueue")
    public void refreshInnerMarketInfo(Date startTime){
        //统计当前时间点与发送消息时间点的差值，如果超过1分钟则告警
        //获取时间毫秒差值
        long diffTime=DateTime.now().getMillis()-new DateTime(startTime).getMillis();
        if (diffTime>60000L){

            log.error("大盘发送消息时间：{}，延迟时间{}ms",new DateTime(startTime).toString("yyyy-MM-dd HH:mm:ss"),diffTime);

        }
        //刷新缓存
        //抵触旧的数据
        caffeineCache.invalidate("innerMarketKey");
        //调用服务方法，刷新数据
        stockService.getInnerMarketInfo();
    }
}