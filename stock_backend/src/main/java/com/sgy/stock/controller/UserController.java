/**
 * @ClassName: UserController
 * @Author: songgaoyuan
 * @Date: 2024-04-15  16:11
 * @Description: 定义用户web层接口资源bean
 * @Version: 1.0
 */
package com.sgy.stock.controller;

import com.sgy.stock.pojo.entity.SysUser;
import com.sgy.stock.service.UserService;
import com.sgy.stock.vo.req.LoginReqVo;
import com.sgy.stock.vo.resp.LoginRespVo;
import com.sgy.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
@Api(tags = "用户相关接口处理器")
@RestController
@RequestMapping("/api")
public class UserController {

    /*
        测试git团队开发
     */
    @Autowired
    private UserService userService;
    /**
     * @author sgy
     * @date 2024-04-15 16:15:03
     * @desc 根据用户名称查询用户信息
     */
    @ApiOperation(value = "根据用户名查询用户信息")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(name = "name",value = "用户名",dataType ="string",required = true,type = "path")
            }
            //type 带问号的请求路径其值为query  /user/{userName}type值为path
    )
    @GetMapping("/user/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String name){

        return userService.findByUserName(name);
    }
    @ApiOperation(value = "用户登录功能")
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo vo){


        return userService.login(vo);
    }
    /**
     * 生成登录校验码的访问接口
     * @return
     */
    @ApiOperation(value = "验证码生成功能")
    @GetMapping("/captcha")
    public R<Map> getCaptchaCode(){
        return userService.getCaptchaCode();
    }

    /**
     * 多条件综合查询用户分页信息，条件包含：分页信息 用户创建日期范围
     */
    @PostMapping("/users")
    public void uses(){

    }
    /**
     * 添加用户信息
     */
    @PostMapping("/user")
    public void adduser(){

    }
    /**
     * 获取用户具有的角色信息，以及所有角色信息
     */
    @GetMapping("/user/roles/{userId}")
    public void getuserInfo(String userId){

    }
    /**
     * 更新用户角色信息
     */
    @PutMapping("/user/roles")
    public void updateuserInfoRole(){

    }
    /**
     *  批量删除用户信息，delete请求可通过请求体携带数据
     */
    @DeleteMapping("/user")
    public void deleteusers(){

    }
    /**
     * 根据用户id查询用户信息
     */
    @GetMapping("/user/info/{userId}")
    public void finduserinfoById(String id){

    }
    /**
     * 根据id更新用户基本信息
     */
    @PutMapping("/user")
    public void updateuserById(){

    }
    /**
     * 分页查询当前角色信息
     */
    @PostMapping("/roles")
    public void findrolepage(){

    }
    /**
     * 树状结构回显权限集合，底层通过递归获取数据集合
     */
    @GetMapping("/permissions/tree/all")
    public void findpermissons(){

    }
    /**
     * 添加角色和角色关联权限
     */
    @PostMapping("/role")
    public void addroleandpermissons(){

    }
    /**
     * 根据角色Id查找对应的权限Id集合
     */
    @GetMapping("/role/{roleId}")
    public void findpermissonidsByroleId(){

    }
    /**
     * 更新角色信息，包含角色关联的权限信息
     */
    @PutMapping("/role")
    public void updateroleinfoandpermissonsrole(){


    }
    /**
     * 根据角色Id删除角色信息
     */
    @DeleteMapping("/role/{roleId}")
    public void updateroleinfo(String roleId){

    }
    /**
     * 更新用户的状态信息
     */
    @PostMapping("/role/{roleId}/{status}")
    public void updateuserstatus(){

    }
    /**
     * 查询所有权限集合
     */
    @GetMapping("/permissions")
    public void findpermissions(){

    }
    /**
     * 添加权限时回显权限树，仅仅显示目录和菜单
     */
    @GetMapping("/permissions/tree")
    public void showmulu(){

    }
    /**
     * 权限添加功能
     */
    @PostMapping("/permission")
    public void addpermission(){

    }
    /**
     * 更新权限
     */
    @PutMapping("/permission")
    public void updatepermission(){}
    /**
     * 删除权限
     */
    @DeleteMapping("/permission/{permissionId}")
    public void deletepermission(){}
    /**
     * 用户操作日志记录功能
     */
}