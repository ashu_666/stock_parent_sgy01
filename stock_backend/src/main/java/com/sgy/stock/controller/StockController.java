/**
 * @ClassName: StockController
 * @Author: songgaoyuan
 * @Date: 2024-04-18  15:31
 * @Description: 定义股票相关接口信息
 * @Version: 1.0
 */
package com.sgy.stock.controller;
import cn.hutool.http.server.HttpServerResponse;
import com.sgy.stock.pojo.domain.*;
import com.sgy.stock.service.StockService;
import com.sgy.stock.vo.resp.PageResult;
import com.sgy.stock.vo.resp.R;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Api(value = "/api/quot", tags = {"分页查询股票最新数据，并按照涨幅排序查询"})
@RestController
@RequestMapping("/api/quot")
public class StockController {
    @Autowired
    private StockService stockService;
    /**
     * @author sgy
     * @date 2024-04-18 15:34:51
     * @desc 获取国内大盘最新的数据
     */

    @ApiOperation(value = "获取国内大盘最新的数据", notes = "获取国内大盘最新的数据", httpMethod = "GET")
    @GetMapping("/index/all")
    public R<List<InnerMarketDomain>> innerIndexAll(){
        return stockService.innerIndexAll();
    }
    /**
     * @author sgy
     * @date 2024-04-19 17:05:53
     * @desc 分页查询股票最新数据，并按照涨幅排序查询
     */

    /**
     *需求说明: 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据
     * @return
     */
    @ApiOperation(value = "获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据", notes = "需求说明: 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据", httpMethod = "GET")
    @GetMapping("/sector/all")
    public R<List<StockBlockDomain>> sectorAll(){
        return stockService.sectorAllLimit();
    }



    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "page", value = ""),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "")
    })
    @ApiOperation(value = "分页查询股票最新数据，并按照涨幅排序查询", notes = "", httpMethod = "GET")
    @GetMapping("/stock/all")
    public R<PageResult<StockUpdownDomain>> getStockInfoByPage(@RequestParam(value = "page",required = false,defaultValue ="1" ) Integer page,
                                                               @RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize){
        return  stockService.getStockInfoByPage(page,pageSize);
    }

    /**
     * 统计最新交易日下股票每分钟涨跌停的数量
     * @return
     */
    @ApiOperation(value = "统计最新交易日下股票每分钟涨跌停的数量", notes = "统计最新交易日下股票每分钟涨跌停的数量", httpMethod = "GET")
    @GetMapping("/stock/updown/count")
    public R<Map> getStockUpdownCount(){
        return stockService.getStockUpdownCount();
    }
    /**
     * 导出指定页码的最新股票信息
     * @param page 当前页
     * @param  pageSize 每页大小
     * @param  response
     */

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "page", value = "当前页"),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "每页大小")
    })
    @ApiOperation(value = "导出指定页码的最新股票信息", notes = "导出指定页码的最新股票信息", httpMethod = "GET")
    @GetMapping("/stock/export")
    public void exportStockUpDownInfo(@RequestParam(value = "page",required = false,defaultValue ="1" ) Integer page,
                                                                  @RequestParam(value = "pageSize",required = false,defaultValue = "20") Integer pageSize,
                                                                  HttpServletResponse response){

      stockService.exportStockUpDownInfo(page,pageSize,response);
    }

    /**
     * 统计大盘T日和T-1日每分钟的交易量
     */

    @ApiOperation(value = "统计大盘T日和T-1日每分钟的交易量", notes = "统计大盘T日和T-1日每分钟的交易量", httpMethod = "GET")
    @GetMapping("/stock/tradeAmt")
    public R<Map<String,List>> getComparedStockTradeAmt(){
        return stockService.getComparedStockTradeAmt();
    }

    /**
     * 统计最新交易时间点下A股在各个涨幅区间的数量
     * @return
     */
    @ApiOperation(value = "统计最新交易时间点下A股在各个涨幅区间的数量(个股涨跌图)", notes = "统计最新交易时间点下A股在各个涨幅区间的数量", httpMethod = "GET")
    @GetMapping("/stock/updown")
    public R<Map> getIncreaseRangeInfo(){
        return stockService.getIncreaseRangeInfo();
    }

    /**
     * 获取指定股票T日的分时数据
     * @param stockCode
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "code", value = "", required = true)
    })
    @ApiOperation(value = "获取指定股票T日的分时数据(个股分时图)", notes = "获取指定股票T日的分时数据", httpMethod = "GET")
    @GetMapping("/stock/screen/time-sharing")
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(@RequestParam(value = "code",required = true)String stockCode){
        return stockService.getStockScreenTimeSharing(stockCode);
    }
    /**查询指定股票每天产生的数据，组装成日K线数据
     * 如果当大盘尚未收盘，则以最新的交易价格作为当天的收盘价格；
     */

    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "string", name = "code", value = "", required = true)
    })
    @ApiOperation(value = "日K图", notes = "如果当大盘尚未收盘，则以最新的交易价格作为当天的收盘价格；", httpMethod = "GET")
    @GetMapping("/stock/screen/dkline")
    public R<List<Stock4EvrDayDomain>> getDayKLinData(@RequestParam("code") String stockCode){
       return stockService.stockCreenDkLine(stockCode);
    }

}