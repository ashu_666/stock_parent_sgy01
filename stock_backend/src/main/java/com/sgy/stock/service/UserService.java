/**
 * @ClassName: UserService
 * @Author: songgaoyuan
 * @Date: 2024-04-15  16:02
 * @Description: 定义用户服务接口
 * @Version: 1.0
 */
package com.sgy.stock.service;

import com.sgy.stock.pojo.entity.SysUser;
import com.sgy.stock.vo.req.LoginReqVo;
import com.sgy.stock.vo.resp.LoginRespVo;
import com.sgy.stock.vo.resp.R;

import java.util.Map;

public interface UserService {


    /**
     * @author sgy
     * @date 2024-04-15 16:03:31
     * @desc 根据用户名称查询用户信息
     */
    
    SysUser findByUserName(String userName);
    /**
     * 用户登录功能实现
     * @param vo
     * @return
     */
    R<LoginRespVo> login(LoginReqVo vo);
    /**
     * 登录校验码生成服务方法
     * @return
     */
    R<Map> getCaptchaCode();
}