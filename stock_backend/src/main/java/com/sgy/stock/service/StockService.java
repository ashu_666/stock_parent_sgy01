/**
 * @ClassName: StockService
 * @Author: songgaoyuan
 * @Date: 2024-04-18  15:36
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.service;

import cn.hutool.http.server.HttpServerResponse;
import com.sgy.stock.pojo.domain.*;
import com.sgy.stock.vo.resp.PageResult;
import com.sgy.stock.vo.resp.R;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


public interface StockService {
    R<List<InnerMarketDomain>> getInnerMarketInfo();
    /**
     * 获取国内大盘的实时数据
     * @return
     */
    R<List<InnerMarketDomain>> innerIndexAll();

    R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize);
    /**
     * 需求说明: 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据
     * @return
     */
    R<List<StockBlockDomain>> sectorAllLimit();
    /**
     * 统计最新交易日下股票每分钟涨跌停的数量
     * @return
     */
    R<Map> getStockUpdownCount();

    void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response);

    R<Map<String, List>> getComparedStockTradeAmt();
    /**
     * 统计最新交易时间点下A股在各个涨幅区间的数量
     */
    R<Map> getIncreaseRangeInfo();

    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);
    /**
     * 单个个股日K 数据查询 ，可以根据时间区间查询数日的K线数据
     *@param stockCode 股票编码
     */
    R<List<Stock4EvrDayDomain>> stockCreenDkLine(String stockCode);
}