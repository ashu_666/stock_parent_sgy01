/**
 * @ClassName: StockServiceImpl
 * @Author: songgaoyuan
 * @Date: 2024-04-18  15:37
 * @Description:
 * @Version: 1.0
 */
package com.sgy.stock.service.impl;

import cn.hutool.core.io.unit.DataUnit;
import cn.hutool.core.lang.hash.Hash;
import cn.hutool.http.server.HttpServerResponse;
import com.alibaba.excel.EasyExcel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sgy.stock.mapper.StockBlockRtInfoMapper;
import com.sgy.stock.mapper.StockMarketIndexInfoMapper;
import com.sgy.stock.mapper.StockRtInfoMapper;
import com.sgy.stock.pojo.domain.*;
import com.sgy.stock.pojo.vo.StockInfoConfig;
import com.sgy.stock.service.StockService;
import com.sgy.stock.utils.DateTimeUtil;
import com.sgy.stock.vo.resp.PageResult;
import com.sgy.stock.vo.resp.R;
import com.sgy.stock.vo.resp.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.tags.EditorAwareTag;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.ServerEndpoint;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.rmi.MarshalledObject;
import java.util.*;
import java.util.stream.Collectors;

@Service("stockService")
@Slf4j
public class StockServiceImpl implements StockService {


    @Autowired
    private StockInfoConfig stockInfoConfig;
    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;
    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;
    @Autowired
    private StockBlockRtInfoMapper stockBlockRtInfoMapper;


    /**
     * 注入本地缓存bean
     */
    @Autowired
    private Cache<String,Object> caffeineCache;
    @Override
    public R<List<InnerMarketDomain>> getInnerMarketInfo() {
        //默认从本地缓存加载数据，如果不存在则从数据库加载并同步到本地缓存
        //在开盘周期内，本地缓存默认有效期1分钟
        R<List<InnerMarketDomain>> result=(R<List<InnerMarketDomain>>) caffeineCache.get(
                "innerMarketKey",key->{
                    //1.获取股票最新交易时间点
                    Date curDate=DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
                    //mock data
                    curDate=DateTime.parse("2022-12-19 15:00:00",DateTimeFormat.forPattern("yyyy-MM-dd HH;mm:ss")).toDate();
                    //2、获取大盘编码集合
                    List<String> mCodes = stockInfoConfig.getInner();
                    //3.调用mapper查询数据
                    List<InnerMarketDomain> data = stockMarketIndexInfoMapper.getMarketInfo(mCodes,curDate);
                    //4.封装并响应
                    return R.ok(data);
                }
        );
        return result;







    }


    @Override
    public R<List<StockBlockDomain>> sectorAllLimit() {

//获取股票最新交易时间点
        Date lastDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //TODO mock数据,后续删除
        lastDate=DateTime.parse("2021-12-21 14:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //1.调用mapper接口获取数据
        List<StockBlockDomain> infos=stockBlockRtInfoMapper.sectorAllLimit(lastDate);
        //2.组装数据
        if (CollectionUtils.isEmpty(infos)) {
            return R.error(ResponseCode.NO_RESPONSE_DATA.getMessage());
        }
        return R.ok(infos);




    }
    @Override
    public R<List<InnerMarketDomain>> innerIndexAll() {
        //1.获取国内A股大盘的Id集合
        List<String> inners=stockInfoConfig.getInner();
        //2.获取最近的股票交易时间
        Date lastDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //3.mock测试数据，后期数据通过第三方接口动态获取数据，可随时删除
        lastDate=DateTime.parse("2021-12-28 09:31:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //3.将获取的Java Date传入接口
        List<InnerMarketDomain> list = stockMarketIndexInfoMapper.getMarketInfo(inners, lastDate);
        return R.ok(list);
    }



    @Override
    public R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize) {
        //1.设置分页参数
        PageHelper.startPage(page,pageSize);
        //2.获取当前最新的股票交易时间点
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //3.todo
        curDate=DateTime.parse("2022-06-07 15:00:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //3.调用mapper接口查询
        List<StockUpdownDomain> infos= stockRtInfoMapper.getNewestStockInfo(curDate);
        if (CollectionUtils.isEmpty(infos)) {
            return R.error(ResponseCode.NO_RESPONSE_DATA);
        }
        //4.组装PageInfo对象，获取分页的具体信息,因为PageInfo包含了丰富的分页信息，而部分分页信息是前端不需要的
        //PageInfo<StockUpdownDomain> pageInfo = new PageInfo<>(infos);
//        PageResult<StockUpdownDomain> pageResult = new PageResult<>(pageInfo);
        PageResult<StockUpdownDomain> pageResult = new PageResult<>(new PageInfo<>(infos));
        //5.封装响应数据
        return R.ok(pageResult);

    }

    @Override
    public R<Map> getStockUpdownCount() {
        //1.获取最新的交易时间范围 openTime  curTime
        //1.1 获取最新股票交易时间点
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //TODO
        curDateTime= DateTime.parse("2022-01-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate=curDateTime.toDate();
        //2.获取最新交易时间点对应的开盘时间点
        Date startDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //3.统计涨停数据
        List<Map> upList= stockRtInfoMapper.getStockUpdownCount(startDate,endDate,1);
        //4.统计跌停
        List<Map> downList= stockRtInfoMapper.getStockUpdownCount(startDate,endDate,0);
        //5.组装数据
        HashMap<Object, Object> info = new HashMap<>();
        info.put("upList",upList);
        info.put("downList",upList);

        return R.ok(info);

    }
    /**
     * @author sgy
     * @param: page pageSize response
     * @desc  导出页码的最新股票信息
     * @return
     */
    @Override
    public void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response) {
        //1.获取分页数据
        R<PageResult<StockUpdownDomain>> r = this.getStockInfoByPage(page,pageSize);
        List<StockUpdownDomain> rows = r.getData().getRows();
        //2.将数据导出到Excel中


        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        try {
            String fileName = URLEncoder.encode("股票信息表", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

            EasyExcel.write(response.getOutputStream(), StockUpdownDomain.class).sheet("模板").doWrite(rows);

        } catch (IOException e) {
            log.error("当前页码：{}，每页大小{}，当前事件：{}，异常信息：{}",page,pageSize,DateTime.now().toString("yyyy-MM-dd HH:mm:ss"),e.getMessage());
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            R<Object> error = R.error(ResponseCode.ERROR);
            String jsonData = null;
            try {
                jsonData = new ObjectMapper().writeValueAsString(error);
                response.getWriter().write(jsonData);

            } catch (IOException ex) {
                log.error("exportStockUpDownInfo响应失败，时间：{}",DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
            }


        }
    }

    @Override
    public R<Map<String, List>> getComparedStockTradeAmt() {
        //1.获取T日 最新股票交易日的日期范围
        DateTime tEndDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        Date tEndDate = tEndDateTime.toDate();
        //todo:mock
        tEndDateTime = DateTime.parse("2022-01-03 09:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

        // 开盘时间
        Date tStartDate = DateTimeUtil.getOpenDate(tEndDateTime).toDate();
        //2.获取t-1日的时间范围
        DateTime preTEndDatetime = DateTimeUtil.getPreviousTradingDay(tEndDateTime);
        //todo:mock
        preTEndDatetime = DateTime.parse("2022-01-02 14:40:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date preTEndDate = preTEndDatetime.toDate();
        //t-1开盘时间
        Date tPreStartDate = DateTimeUtil.getOpenDate(preTEndDatetime).toDate();
        //Todo:测试数据


        //3.调用mapper查询
        //3.1 统计t日
        List<Map> tData = stockMarketIndexInfoMapper.getSumAmtInfo(tStartDate, tEndDate, stockInfoConfig.getInner());
        //3.2 统计t-1日

        List<Map> pretData = stockMarketIndexInfoMapper.getSumAmtInfo(tPreStartDate, preTEndDate, stockInfoConfig.getInner());

        //4.组装数据
        HashMap<String, List> info = new HashMap<>();

        info.put("amtList", tData);
        info.put("yesAmtList", pretData);
        //5.响应数据
        return R.ok(info);

    }

    /**
     * 统计最新交易时间点下A股在各个涨幅区间的数量
     */
    @Override
    public R<Map> getIncreaseRangeInfo() {
        //1.获取当前最新的股票交易时间点
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //todo：mock data
        curDateTime=DateTime.parse("2022-01-06 09:55:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate=curDateTime.toDate();
        //2.调用mapper获取数据
        List<Map> infos= stockRtInfoMapper.getIncreaseRangeInfoByDate(curDate);
        //获取yml中属性 有序的涨幅区间标题集合
        List<String> updownrange=  stockInfoConfig.getUpDownRange();

        //将顺序的涨幅区间内的每个元素转化为Map对象
        //方式一：普通循环
//        List<Map> allInfos=new ArrayList<>();
//        for(String title:updownrange){
//            Map tmp=null;
//            for (Map info:infos){
//                if (info.containsValue(title)){
//                    tmp=info;
//                    break;
//                }
//            }
//            if (tmp==null){
//                //不存在，则补齐
//                tmp=new HashMap();
//                tmp.put("count",0);
//                tmp.put("title",title);
//            }
//            allInfos.add(tmp);
//        }
        //方式2.stream流
        List<Map> allInfos = updownrange.stream().map(title -> {
            Optional<Map> result = infos.stream().filter(map -> map.containsValue(title)).findFirst();
            if (result.isPresent()) {
                return result.get();
            } else {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp.put("count", 0);
                tmp.put("title", title);
                return tmp;
            }
        }).collect(Collectors.toList());
        //3.组装数据
        HashMap<String, Object> data = new HashMap<>();
        data.put("time",curDateTime.toString("yyyy-MM-dd HH:mm:ss"));
//        data.put("infos",infos);
        data.put("infos",allInfos);
        return R.ok(data);
    }
    /**
     * 获取指定股票T日的分时数据
     * @param stockCode 股票编码
     * @return
     */
    @Override
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode) {
        //1.获取最近最新的交易时间点和对应的开盘日期
        //1.1获取最近有效时间点
        DateTime lastDate4Stock= DateTimeUtil.getLastDate4Stock((DateTime.now()));
        Date endTime = lastDate4Stock.toDate();
        //todo：mockdata
        endTime= DateTime.parse("2021-12-30 14:47:00",DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();

        //1.2获取最近有效时间点对应的开盘日期
        DateTime openDateTime = DateTimeUtil.getOpenDate(lastDate4Stock);
        Date startTime=openDateTime.toDate();
        //todo：mockdata
        startTime=DateTime.parse("2021-12-30 09:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //2、根据股票code和日期范围查询
        List<Stock4MinuteDomain> list= stockRtInfoMapper.getStockInfoByCodeAndDate(stockCode,startTime,endTime);
        //判断非空处理
        if(CollectionUtils.isEmpty(list)){
            list=new ArrayList<>();
        }
        //返回响应数据
        return R.ok(list);
    }
    /**
     * 功能描述：单个个股日K数据查询 ，可以根据时间区间查询数日的K线数据
     * 		默认查询历史20天的数据；
     * @param stockCode 股票编码
     * @return
     */
    @Override
    public R<List<Stock4EvrDayDomain>> stockCreenDkLine(String stockCode) {
        //1.获取查询的日期范围
        //1.1 获取截止时间
        DateTime endDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        Date endTime = endDateTime.toDate();
        //TODO MOCKDATA
        endTime=DateTime.parse("2022-06-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //1.2 获取开始时间
        DateTime startDateTime = endDateTime.minusDays(10);
        Date startTime = startDateTime.toDate();
        //TODO MOCKDATA
        startTime=DateTime.parse("2022-01-01 09:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //2.调用mapper接口获取查询的集合信息-方案1
        List<Stock4EvrDayDomain> data= stockRtInfoMapper.getStockInfo4EvrDay(stockCode,startTime,endTime);
        //3.组装数据，响应
        return R.ok(data);






    }
}